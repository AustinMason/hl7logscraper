/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hl7logscraper;

import java.io.*;
import java.io.BufferedReader;
import java.util.*;
//import au.com.kegareuh.mako.*;


/**
 *
 * @author amason
 */
public class HL7LogScraper {
    
    private static cmdLineArguements theArguements;
    private static List HL7Messages;
    private final static char CR  = (char) 0x0D;
    private final static char LF  = (char) 0x0A;
    private static List HL7MessagesOut;
    

    
    public static void main(String[] args) throws Exception
    {
        System.out.println("HL7 Log Scraper\n");
        
        
        if(args.length == 0 || args[0].contains("?") )
        {
            displayHelp();
            return;
        }
        theArguements = new cmdLineArguements(args);
        

        String logPath = theArguements.getArgument("logs");
        
        
        List allTheFilePaths = new ArrayList();
        getFileListRecursive(allTheFilePaths, logPath);
        
       listFiles(allTheFilePaths);
        
        if(theArguements.getArgument("filef").length() > 0)
        {
            filterFiles(allTheFilePaths, theArguements.getArgument("filef"));
            
        }
       
        listFiles(allTheFilePaths);
        
        if(theArguements.getArgument("list").length() > 0)
        {
            System.out.println("Listing files to read only.... re-run without list=blaaa switch to run");
            return;
        }
        
        System.out.println("Loading HL7 messages..........");
        
        HL7Messages = new ArrayList();
        HL7MessagesOut = new ArrayList();
        
        for(int i = 0; i < allTheFilePaths.size(); i++)
        {
            List thisOneFilePath = new ArrayList();
            thisOneFilePath.add( allTheFilePaths.get(i) );// hack to only process 1 file at a time
            loadHL7Messages(thisOneFilePath);
            System.out.println("Number HL7 messages loaded: "+ HL7Messages.size() );
            removeMessgaesFromList();
            
            for(int j = 0; j < HL7Messages.size(); j ++)
            {
                HL7MessagesOut.add( HL7Messages.get(j));
            }
            HL7Messages.clear();
        }
            

            
        
            
        
        


        writeToOutputFile();
  
        
        
    }
    public static void writeToOutputFile() throws Exception
    {
        String outFile = theArguements.getArgument("out");
        
        BufferedWriter output = null;
        System.out.println("Writing to output file:" +outFile );
        System.out.println( "Number of messages to be written:" + HL7MessagesOut.size() );
        try 
        {
            File file = new File(outFile);
            output = new BufferedWriter(new FileWriter(file));
            
            for(int i = 0; i < HL7MessagesOut.size(); i++ )               
            {
            
                output.write( ""+HL7MessagesOut.get(i) );
                output.write("\r\n");
            }
            
            
        } 
        catch ( IOException e ) 
        {
            e.printStackTrace();
        } 
        finally 
        {
            if ( output != null ) 
            {
                output.close();
            }
            }
    }
    
    public static boolean isA28ForWestern(String msg)
    {
        
            int endOfMSH = msg.indexOf(CR);
            //System.out.println("end of msh is at char#"+endOfMSH);
            
            String hl7MsgLineMSH = msg.substring(0, endOfMSH);
            if( hl7MsgLineMSH.contains("ADT^A28") ) return true;
            
            return false;
            
        
    
    }


         
        
        
        
       
    public static void removeMessgaesFromList()
    {
        for(int i = HL7Messages.size()-1; i >= 0 ; i--)
        {//traverse list backwards as we are removing items as we go
            String msg = ""+HL7Messages.get(i);
          /*  if(!isWesternSIUReplayMSG(msg))
            {
                HL7Messages.remove(i);
            }*/
            if(!isA28ForWestern(msg)) HL7Messages.remove(i);
            
            
        }
    }
        
            

    public static boolean isWesternSIUReplayMSG(String msg)
    {
     
            int endOfMSH = msg.indexOf(CR);
            //System.out.println("end of msh is at char#"+endOfMSH);
            
            String hl7MsgLineMSH = msg.substring(0, endOfMSH);
            if( !hl7MsgLineMSH.contains("SIU^") ) return false;
            //if(msg.indexOf("PV1") == -1) return false;
            
            if( msg.contains("CSDFSDP") || msg.contains(("CSDFSMEL") )) return true;
        /*    int startPV1 = msg.indexOf("PV1|");
            String msgFromPV1onwards = msg.substring(startPV1, msg.length());
            int endOfPv1 = msgFromPV1onwards.indexOf(CR);
            String hl7MsgLinePV1 = msgFromPV1onwards.substring(0,endOfPv1);
            String[] PV1 = hl7MsgLinePV1.split("[|]");
            if(PV1[3].startsWith("CSDFSDP") || PV1[3].startsWith("CSDFSMEL") )
                {
                System.out.println("MSG Found with clinic code CSDFSDP or CSDFSMEL");
                return true;
                    
            }*/
            //String hl7MsgMinusMSH = msg.substring( endOfMSH, msg.length() );
            //System.out.println("MSH="+hl7MsgMinusMSH);
            //int endOfSCH = msg.indexOf(CR);
           // String hl7MsgLineSCH = hl7MsgMinusMSH.substring(0, endOfSCH);
             //System.out.println("SCH="+hl7MsgLineSCH);
          //  String[] SCH = hl7MsgLineSCH.split("[|]");
           //if(SCH[5].startsWith("CSDFSDP") || SCH[5].startsWith("CSDFSMEL") ) 
            
            
            return false;
            
        
 
    }
    
    public static void loadHL7Messages(List fileList) throws Exception
    {
        for(int i = 0; i < fileList.size(); i++)
        {
            boolean readingMsgLine = false;
            String hl7Msg = "";
            try
            {
                BufferedReader br = new BufferedReader(new FileReader( ""+fileList.get(i) ));

                String line = br.readLine();

                while (line != null) 
                {
                    if( line.startsWith("MSH|")  )
                    {
                        readingMsgLine = true;
                    }
                    if(readingMsgLine)
                    {
                        line = line + CR;
                        hl7Msg = hl7Msg + line;
                    }
                    if(readingMsgLine && (!line.contains("|") || line.length() < 3))
                    {// dont add short messages or messages that are acks!
                        readingMsgLine = false;
                        if(hl7Msg.length() > 50)
                        {
                            if( !hl7Msg.contains("MSA|") ) 
                            {
                               // System.out.println("this is the message i read: "+hl7Msg);
                                addHL7MsgFromString(hl7Msg);
                                
                            }
                        }
                        hl7Msg = "";
    
                        
                    }

                    line = br.readLine();
                }
                readingMsgLine = false;
                br.close();
             }
            catch ( Exception ex ){
               // do nothing
            }
            
            
         


    }
    }
    
public static void addHL7MsgFromString(String msg)
    {
        try {
            //Mako HL7 = new Mako(msg);
            HL7Messages.add(msg);
           }
        catch ( Exception ex )
        {
            // do nothing

        }
        
        System.out.println("Adding HL7 msg #"+HL7Messages.size() );
        
        
        
    }
    
    
    public static void listFiles(List fileList)
    {
         for(int i = 0; i < fileList.size(); i++)
        {
                System.out.println( "" + fileList.get(i) );
        }
         System.out.println( "The number of files in list is:" + fileList.size() );
        
        
    }
    
    public static void filterFiles(List fileList, String filterTxt)
    {
        System.out.println( "Filtering File list by text" + filterTxt);
        for(int i=fileList.size() -1; i >= 0 ; i--)
        {
            String thisFile = ""+fileList.get(i);
            if(!thisFile.contains( filterTxt ))
            {
                fileList.remove(i);

            }

        }
        System.out.println( "completed Filtering Function" );
            
        
        
    }
    public static void displayHelp()
    {
         System.out.println( "Command Arguements: HL7LogScraper.jar logs=[log File Path] filef=[file filter text] out=[outputfile] list=[anything to list]");
         System.out.println( "Example Use: java -jar HL7LogScraper.jar logs=c:\\Temp filef=IPM out=c:\\output.txt list=yes");
         System.out.println( "Also, if your running out of memory use: -Xmx1024m like this: java -Xmx1024m -jar HL7LogScraper.jar logs=c:\\Temp filef=IPM out=c:\\output.txt");
        
        
    
    }
    
    public static void getFileListRecursive(List addToThisList, String dir)
    {
        System.out.println("Finding Files...Scanning directory:" + dir );
        
            
        File folder = new File(dir);
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++)
        {
          if (listOfFiles[i].isFile()) 
          {
            
            addToThisList.add( listOfFiles[i].getAbsolutePath() );
            //System.out.println("File " + listOfFiles[i].getName());
                
          } 
          else if (listOfFiles[i].isDirectory()) 
          {
              getFileListRecursive(addToThisList, listOfFiles[i].getAbsolutePath() );
              
            //System.out.println("Directory " + listOfFiles[i].getName());
          }
        }
            
        
        
    }
        
        
 
    
}
